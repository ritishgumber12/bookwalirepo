function setwarning(tid,m){
	$("#"+tid).find('span').html(" "+m);
	$("#"+tid).css({"display":"block"});
}
function hidewarning(tid){
	$("#"+tid).find('span').html(" ");
	$("#"+tid).css({"display":"none"});
}

$(document).ready(function(){

	window.isanyfileup=0;
	$(".formwarningtext").css({"display":"none"});
var current_fs, next_fs, previous_fs;
$(".next").click(function(){
	
	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
/*
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
			current_fs.hide();
			next_fs.show();

*/
		if($(this).hasClass("fileuploadnext")){
				if(window.isanyfileup>=1){
					$("#fileuploadwarningtext").css({"display":"none"});
					$('input').blur();
					current_fs = $(this).parent();
					next_fs = $(this).parent().next();
					$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
				      current_fs.hide();
				      next_fs.show();
								$('select option').filter(function() {
    									return !this.value || $.trim(this.value).length == 0 || $.trim(this.text).length == 0;
								}).remove();
				}else{
					setwarning("fileuploadwarningtext"," Upload atleast one file.");
				}
		}else if($(this).hasClass("morebookdet")){
				if($("#booknameinput").val()!=''){
					if($("#authorinput").val()!=''){
						if($("#descinput").val()!=''){
							if($("#editioninput").val()!=''){
									if($("#priceinput").val()!=''){
										if($("input[name=isOld]:checked").length>0){
											if($("input[name=fd]:checked").length>0){
												if($("select[name='catselection'] option:selected").length>0){
														$("#bookdetailswarningtext").css({"display":"none"});
														current_fs = $(this).parent();
														next_fs = $(this).parent().next();
														$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
													      current_fs.hide();
													      next_fs.show();
												}else{
													setwarning("bookdetailswarningtext","Choose a specific category.");
												}
											}else{
												setwarning("bookdetailswarningtext","Choose a delivery preference.");
											}
										}else{
											setwarning("bookdetailswarningtext","Choose a book type.");
										}
									}else{
										setwarning("bookdetailswarningtext","Enter a valid price of the book.");
									}
							}else{
								setwarning("bookdetailswarningtext","Enter a valid edition of the book.");
							}
						}else{
							setwarning("bookdetailswarningtext","Enter a valid description of the book.");
						}
					}else{
						setwarning("bookdetailswarningtext","Enter a valid author.");
					}
				}else{
						setwarning("bookdetailswarningtext","Enter a book name.");
				}
		}

});

$(".previous").click(function(){
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
  current_fs.hide();
  previous_fs.show();
});

$(".submit").click(function(){
	return false;
});
});
