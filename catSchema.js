var mongoose = require("mongoose");
var catSchema = {
    title: {
        required: true,
        type: String
    },
	image: {
		type:String,
		required:true
	}
};
var schema = new mongoose.Schema(catSchema);
module.exports = schema;
module.exports.catSchema = schema;