var mongoose = require("mongoose");
var testSchema = {
    pid: {
        required: true,
        type: Number
    }};
var schema = new mongoose.Schema(testSchema);
module.exports = schema;
module.exports.testSchema = schema;