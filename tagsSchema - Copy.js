var mongoose = require("mongoose");
var sessionSchema = {
	userId:
	{
		type: mongoose.Schema.ObjectId,
		ref: 'User'
	},
	token:
	{
		type: String
	},
	lastUpdated:
	{
		type: Number
	},
};
var schema = new mongoose.Schema(sessionSchema);
module.exports = schema;
module.exports.sessionSchema = schema;
