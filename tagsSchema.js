var mongoose = require("mongoose");
var tagsSchema = {
    tag: {
        required: true,
        type: String
    }
};
var schema = new mongoose.Schema(tagsSchema);
module.exports = schema;
module.exports.tagsSchema = schema;