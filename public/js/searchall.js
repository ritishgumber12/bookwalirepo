$(document).ready(function(){
  $('#costfilter').on('change mousemove', function(){
    $('#costfiltertext').text("Rs. " + $('#costfilter').val());
});
$('#radiusfilter').on('change mousemove', function(){
  $('#radiusfiltertext').text($('#radiusfilter').val() + " K.m.");
});
$(".checkboxrow").hover(function(){
   $(this).css({"background":"#e5e5e5"});
 },
  function(){
    $(this).css({"background":"#fff"});

  }
);
$(".checkboxrow").click(function(){
  var checkboxobject = $(this).find('input')
  if(checkboxobject.is(":checked")){
    checkboxobject.prop('checked',false);
  }else{
    checkboxobject.prop('checked',true);
  }
});
});
