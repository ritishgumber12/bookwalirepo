var mongoose = require("mongoose");
var listingSchema = {
	bookType:
	{ //old=true or new
		type: Boolean,
		default: true,
		required: true
	},
	images: [],
	title:String,
	category:String,
	lat:
	{
		type: Number,
		required: true
	},
	lon: 	
	{
		type: Number,
		required: true
	},
	city:
	{
		type: String,
		required: true
	},
	price:
	{
		type: Number,
		required: true,
		default: 0
	},
	soldBy:
	{
		type: String,
		required: true
	},
	postedOn:
	{
		type: String,
		required: true
	},
	status:
	{ //sold=true
		type: Boolean,
		required: true,
		default: false
	},
	edition:
	{
		type: Number
	},
	trendCount:
	{
		type: Number
	},
	featured:
	{
		type: Boolean,
		default: false
	},
	freeDelivery:
	{//true=yes
	
		type: Boolean,
	},
	pageId:
	{
		type: mongoose.Schema.ObjectId,
		ref: 'page'
	},
	approved:{
	type:Boolean,
	default:false},
	reviews:[],
	isbn:{
	type:Number}
};
var schema = new mongoose.Schema(listingSchema);
module.exports = schema;
module.exports.listingSchema = schema;
