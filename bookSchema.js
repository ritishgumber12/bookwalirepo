var mongoose = require("mongoose");
var bookSchema = {
tags:[],
    name: {
        required: true,
        type: String
    },
    bookType: { //old or new
        type: Boolean,
        default: true,
        required: true
    },
    images: [],
    category: {
        type: String,
    },
    lat: {
        type: Number,
		required: true
    },
    lon: {
        type: Number,
		required: true
    },
	city:{
		type:String,
		required:true
	},
    price: {
        type: Number,
        required: true,
        default: 0
    },
    soldBy: {
        type: String,
        required: true
    },
    postedOn: {
        type: String,
        required: true
    },
    status: { //sold
        type: Boolean,
        required: true,
        default: false
    },
    course: {
        type: String,
        required: true
    },
    year: {
        type: Number
    }
};
var schema = new mongoose.Schema(bookSchema);
module.exports = schema;
module.exports.bookSchema = schema;