function call() {
	console.log("called");
        var files = document.getElementById("file_input").files;
        var file = files[0];
        var x=file.type;
		var y=x.toString();
		console.log(y);
		if(file == null){
            alert("No file selected.");
        }
        else if(y.indexOf("image/")==-1)
		alert("Only images are accepted");
		else{
		console.log(file.type);
		//document.getElementById('status').innerHtml="Please Wait";
            get_signed_request(file);
        }
    };

function get_signed_request(file){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/sign_s3?file_name="+file.name+"&file_type="+file.type+"&");
    xhr.onreadystatechange = function(){
        if(xhr.readyState === 4){
            if(xhr.status === 200){
                var response = JSON.parse(xhr.responseText);
                upload_file(file, response.signed_request, response.url);
            }
            else{
                alert("Could not get signed URL.");
            }
        }
    };
    xhr.send();
}
function upload_file(file, signed_request, url){
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", signed_request);
    xhr.setRequestHeader('x-amz-acl', 'public-read');
    xhr.onload = function() {
        if (xhr.status === 200) {
           document.getElementById("preview").src = url;
		   
          //  document.getElementById("url").innerHTML= url;
			
        }
    };
    xhr.onerror = function() {
        alert("Could not upload file.");
    };
    xhr.send(file);
}