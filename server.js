var express = require("express");
var configAuth = require('./config/auth');
var passport = require('passport');
var mongoose = require("mongoose");
var path = require("path");
var busboy = require('connect-busboy');
var aws = require('aws-sdk');
var expressSession = require('express-session');
mongoose.connect("mongodb://bookadda:ritish12@ds011389.mlab.com:11389/heroku_07nnswqs");
//mongoose.connect("mongodb://127.0.0.1:27017/SaveBook");
var userSchema = require("./userSchema");
userSchema.methods.setPassword = function(password)
{
	this.salt = crypto.randomBytes(16)
		.toString('hex');
	this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64)
		.toString('hex');
};
userSchema.methods.validPassword = function(password)
{
	var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64)
		.toString('hex');
	return this.hash === hash;
};
userSchema.methods.generateJwt = function()
{
	var expiry = new Date();
	console.log(expiry);

	// expiry.setDate(expiry.getDate() + 7);
	return jwt.sign(
	{
		_id: this._id,
		email: this.email,
		name: this.name,
		dp:this.dp,
		exp: parseInt((expiry.getTime() + 2 * 60 * 60 * 1000) / 1000),
	}, String(process.env.Secret)); // DO NOT KEEP YOUR SECRET IN THE CODE!
};
userSchema.methods.generateJwt2 = function(x)
{
	var expiry = new Date();
	console.log(expiry);
	// expiry.setDate(expiry.getDate() + 7);

	return jwt.sign(
	{
		_id: this._id,
		dp:this.dp,
		providerid: this.providerid[this.loginProvider.indexOf(x)],
		loginProvider:x,
		name: this.name,
		exp: parseInt((expiry.getTime() + 2 * 60 * 60 * 1000) / 1000),
	}, String(process.env.Secret)); // DO NOT KEEP YOUR SECRET IN THE CODE!
};
var Listing = mongoose.model("Listing", require("./listingSchema"), "listing");
var User = mongoose.model("User", userSchema, "users");
var Admin = mongoose.model("Admin", require("./adminSchema"), "admin");
var Page = mongoose.model("Page", require("./pageSchema"), "page");
var Tags = mongoose.model("Tags", require("./tagsSchema", "tags"));
var Session = mongoose.model("Session", require("./sessionSchema", "session"));
var tree = require("mongoose-path-tree");
var CatSchema = require("./catSchema");
CatSchema.plugin(tree);
var Category = mongoose.model("Category", CatSchema, "category");
var geocoderProvider = 'google';
var httpAdapter = 'https';
var extra = {};
var geocoder = require('node-geocoder')(geocoderProvider, httpAdapter, extra);
var crypto = require('crypto'),
	algorithm = 'aes-256-ctr',
	password = 'd6F3Efeq';
var jwt = require('jsonwebtoken');
var Test = mongoose.model("Test", require("./testSchema"), "test2");

function distanc(lat1, lon1, lat2, lon2)
{
	var R = 6371; // Radius of the earth in km
	var dLat = deg2rad(lat2 - lat1); // deg2rad below
	var dLon = deg2rad(lon2 - lon1);
	var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	var d = R * c; // Distance in km
	return d;
};

function deg2rad(deg)
{
	return deg * (Math.PI / 180)
}
module.exports = function()
{
	var app = express();
	app.use(passport.initialize());
	app.use(expressSession(
	{
		secret: 'ok'
	}));
	var apiRoutes = express.Router();
	app.set('superSecret', process.env.Secret); // secret variable
	app.use('/', express.static(path.join(__dirname, 'public')));
	app.use('/api', apiRoutes);
	app.use(require('nodejs-fastload')
		.loader());
	var bodyParser = require('body-parser')
	app.use(bodyParser.json()); // to support JSON-encoded bodies
	app.use(bodyParser.urlencoded(
	{ // to support URL-encoded bodies
		extended: true
	}));
	var AWS_ACCESS_KEY = 'AKIAICKFK47LKTGIF5ZA';
	var AWS_SECRET_KEY = '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';
	var S3_BUCKET = 'bookxdata';
	app.set('port', process.env.PORT || 8888);
	app.engine('html', require('ejs')
		.renderFile);
	apiRoutes.use(function(req, res, next)
	{
		var token = req.query.lt;
		//		var token = req.body.token || req.query.token || req.headers['x-access-token'];
		token = decrypt(token);
		if (token)
		{
			jwt.verify(token, app.get('superSecret'), function(err, decoded)
			{
				if (err)
				{
					return res.json(
					{
						valid: false,
						message: 'Failed to authenticate token.'
					});
				}
				else
				{
					req.decoded = decoded;
					next();
				}
			});
		}
		else
		{
			console.log("here");
			// if there is no token
			return res.status(403)
				.send(
				{
					valid: false
				});
		}
	});
	apiRoutes.post("/authenticate", function(req, res)
	{
		console.log("authenticate 1");
		var userId = req.decoded._id;
		Session.findOne(
		{
			userId: userId,
			token:
			{
				$in: [req.query.lt]
			}
		}, function(err, session)
		{
			if (err) console.log(err);
			if (session)
			{
				User.findById(userId, function(err, user)
				{
					if (err) console.log(err);
					res.send(
					{
						valid: true,
						doc: user
					});
				});
			}
			else
			{
				res.send(
				{
					valid: false,
				});
			}
		});
	});
	require("./config/passport")(passport, User,app);
	require("./routes/socialLoginRoutes")(app,passport);


	app.get('/', function(req, res)
	{
		res.render("./search.html");
	});
	app.get('/enquiry', function(req, res)
	{
		res.render("./enquiry.html");
	});
	app.get('/addBook', function(req, res)
	{
		res.render("./addBook.html");
	});
	app.get('/bookEnquiry', function(req, res)
	{
		res.send('');
	});
	app.get('/addNewBook', function(req, res)
	{
		res.render("./addNewBook.html");
	});
	app.get('/addExistingBook', function(req, res)
	{
		res.render("./addExisting.html");
	});
	app.get("/logout",function(req,res){
		res.logout();
	});
	app.get('/test', function(req, res)
	{
		Test.find(
		{}, function(err, docs)
		{
			var x = docs.length;
			var response = [];
			var pids = [];
			console.log(x);
			docs.forEach(function(doc)
			{
				if (pids.indexOf(doc.pid) == -1)
				{
					response.push(
					{
						pid: doc.pid,
						n: 1
					});
					pids.push(doc.pid);
					x--;
					if (x == 0)
					{
						res.send(response);
					}
				}
				else
				{
					console.log("else");
					response[pids.indexOf(doc.pid)].n += 1;
					x--;
					if (x == 0)
					{
						res.send(response);
					}
				}
			});
		});
	});
	app.get('/addCat', function(req, res)
	{
		res.render("./addCategory.html");
	});
	app.get('/product', function(req, res)
	{
		res.render("./productpg.html");
	});
	app.get('/myBook', function(req, res)
	{
		res.render("./mybooks.html");
	});
	app.get('/editlisting', function(req, res)
	{
		res.render("./editlisting.html");
	});
	app.get("/updateBook", function(req, res)
	{
		Listing.update(
		{
			_id: req.query.id
		},
		{
			price: parseInt(req.query.price),
			status: req.query.status
		}, function(err, doc)
		{
			console.log(doc);
		});
	});
	app.get('/login', function(req, res)
	{
		res.render("./login.html");
	});
	app.get('/register', function(req, res)
	{
		res.render("./register.html");
	});
	app.post('/addUser', function(req, res)
	{
		console.log("inside addUser");
		
			User.count(
			{
				email: (req.query.email)
					.toLowerCase()
			}, function(err, c)
			{
				if (err)
				{
					console.log(err);
					process.exit(1);
				}
				console.log(c);
				if (c == 0)
				{
					var user = new User(
					{
						name: req.query.uname,
						email: req.query.email,
						cno: req.query.cno,
						dp:"http://previews.123rf.com/images/bamulahija/bamulahija1208/bamulahija120800001/14930262-Funny-Baba-Stock-Vector-baba-sadhu.jpg",
						username: req.query.username
					});
					user.setPassword(req.query.pass);
					user.save(function(error, resp)
					{
						if (error)
						{
							console.log(error);
							process.exit(1);
						}
						else
						{
							console.log(resp);
							var token;
							token = user.generateJwt();
							res.status(200);
							res.json(
							{
								"success": true,
								"token": token
							});
						}
					});
				}
				else
				{

					res.send("User already Exists");
				}
			});
			});
	app.get("/checkEmail", function(req, res)
	{
		User.count(
		{
			email: req.query.email
		}, function(err, c)
		{
			if (err) console.log(err);
			else
			{
				if (c == 0) res.send(
				{
					status: true
				});
				else res.send(
				{
					status: false
				});
			}
		});
	});
	app.get("/checkUsername", function(req, res)
	{
		User.count(
		{
			username: req.query.username
		}, function(err, c)
		{
			if (err) console.log(err);
			else
			{
				if (c == 0) res.send(
				{
					status: true
				});
				else res.send(
				{
					status: false
				});
			}
		});
	});
	app.get("/categories", function(req, res)
	{
		res.render("./categories.html");
	});
	app.get("/book", function(req, res)
	{
		res.render("./bookpage.html");
	});
	app.get("/findtherightbook", function(req, res)
	{
		res.render("./ftrb.html");
	});
	app.get("/user", function(req, res)
	{
		res.render("./user.html");
	});
	app.get("/searchBook", function(req, res)
	{
		res.render("./search.html");
	});
	app.get("/search", function(req, res)
	{
		res.render("./searchAll.html");
	});
	app.get("/profile", function(req, res)
	{
		res.render("./userProfile.html");
	});
	app.get("/allRecords", function(req, res)
	{
		res.render("./all.html");
	});
	app.post("/save", function(req, res)
	{
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1; //January is 0!
		var yyyy = today.getFullYear();
		if (dd < 10)
		{
			dd = '0' + dd
		}
		if (mm < 10)
		{
			mm = '0' + mm
		}
		var x = req.query.imgUrl;
		var img = x.split("$$");
		x = req.query.tags;
		var tags = x.split("$$");
		var todayy = dd + '/' + mm + '/' + yyyy;
		var pageId = req.query.pageId;
		User.findOne(
		{
			email: req.query.email
		}, function(err, user)
		{
			if (err) console.log(err);
			else if (user != null)
			{
				var listing = new Listing(
				{
					bookType: req.query.bookType,
					images: img,
					lat: user.lat,
					lon: user.lon,
					city: user.city,
					price: req.query.price,
					soldBy: req.query.email,
					postedOn: todayy,
					edition: req.query.edition,
					category: req.query.category,
					trendCount: 0,
					title: req.query.name,
					freeDelivery: req.query.fd
				});
				listing.save(function(err, doc)
				{
					user.listingIds.push(doc._id);
					user.markModified('listingIds');
					user.save();
					if (err) console.log(err);
					if (pageId == null || pageId == 'undefined' || pageId == '')
					{
						var page = new Page(
						{
							title: req.query.name,
							author: req.query.author,
							tags: tags,
							description: req.query.desc,
							category: req.query.category,
							listings: [doc._id],
							coverImage: img[0]
						});
						page.save(function(err, docs)
						{
							if (err) console.log(err);
							console.log(docs);
							doc.pageId = docs._id;
							doc.save();
							res.send(docs);
						});
					}
					else
					{
						Page.findOne(
						{
							_id: pageId
						}, function(err, page)
						{
							var x = page.listings;
							x.push(doc._id);
							page.listings = x;
							var x = page.tags;
							tags.forEach(function(tag)
							{
								x.push(tag);
							});
							page.tags = x;
							doc.pageId = page._id;
							doc.save();
							page.save(function(err, p)
							{
								if (err) console.log(err);
								console.log(p);
								res.send(p);
							});
						});
					}
				});
			}
			else
			{
				res.send("error saving");
			}
		});
		tags.forEach(function(tag)
		{
			Tags.count(
			{
				tag:
				{
					$regex: tag,
					$options: "$i"
				}
			}, function(err, c)
			{
				if (c == 0)
				{
					var a = new Tags(
					{
						tag: tag
					});
					a.save(function(err, doc)
					{
						if (err) console.log(err);
						console.log(doc);
					})
				}
			});
		});
	});
	app.get("/deleteTag", function(req, res)
	{
		var tag = req.query.tag;
		var id = req.query.id;
		var index = req.query.index;
		var type = req.query.type;
		if (type == 1)
		{
			Tags.findOne(
			{
				tag:
				{
					$regex: tag,
					$options: "$i"
				}
			}, function(err, doc)
			{
				if (err) console.log(err);
				doc.remove();
			});
		}
		Page.findById(id, function(err, doc)
		{
			var x = doc.tags;
			x.splice(index, 1);
			doc.tags = x;
			doc.save();
			res.send("done");
		});
	});
	app.get("/updateCoverImage", function(req, res)
	{
		var id = req.query.id;
		var url = req.query.url;
		Page.findById(id, function(err, doc)
		{
			if (err) console.log(err);
			doc.coverImage = url;
			doc.save(function(err, a)
			{
				if (err) console.lof(err);
				res.send(a)
			});
		});
	});
	app.get("/all", function(req, res)
	{
		var ski = req.query.skip;
		var x = (req.query.searchText)
			.split(" ");
		var patt = "(.*)";
		for (var i = 0; i < x.length; i++) patt += x[i] + "(.*)";
		var countt;
		var sent = false;
		var priceFilter = req.query.priceFilter;
		var ob = req.query.ob;
		var fd = req.query.fd;
		if (req.query.type == 1)
		{
			Listing.find(
				{
					title:
					{
						$regex: patt,
						$options: "$i"
					},
					approved: true,
					price:
					{
						$gt: 0,
						$lt: parseInt(priceFilter)
					},
					bookType: ob,
					status: false,
					freeDelivery: fd
				}, function(error, docs)
				{
					sent = true;
					res.header("Access-Control-Allow-Origin", "*");
					res.send(docs);
				})
				.sort('-trendCount')
				.skip(parseInt(ski))
				.limit(10);
		}
		else
		{
			Listing.find(
				{
					title:
					{
						$regex: patt,
						$options: "$i"
					},
					approved: true,
					price:
					{
						$gt: 0,
						$lt: parseInt(priceFilter)
					},
					status: false,
				}, function(error, docs)
				{
					sent = true;
					res.header("Access-Control-Allow-Origin", "*");
					res.send(docs);
				})
				.sort('-trendCount')
				.skip(parseInt(ski))
				.limit(10);
		}
	});
	app.get('/checkUser', function(req, res)
	{
		User.findOne(
		{
			email: (req.query.email)
				.toLowerCase()
		}, function(err, user)
		{
			res.header("Access-Control-Allow-Origin", "*");
			passport.authenticate('local', function(err, user, info)
			{
				var token;
				// If Passport throws/catches an error
				if (err)
				{
					res.status(404)
						.json(err);
					return;
				}
				// If a user is found
				if (user)
				{
					token = user.generateJwt();
					res.status(200);
					res.redirect("/addCookie#?token="+token);

				}
				else
				{
					// If user is not found
					res.status(401)
						.json(info);
				}
			})(req, res);
			/*if (docs != null)
				{
					if (docs.password == req.query.password)
					{
						var token = jwt.sign(
						{
							_id: docs._id,
							password: docs.password,
							time: (new Date())
								.getTime()
						}, app.get('superSecret'),
						{
							expiresIn: '1h' // expires in 1 hour
						});
						token = encrypt(token);
						Session.findOne(
						{
							userId: docs._id
						}, function(err, sess)
						{
							if (sess)
							{
								sess.token.push(token);
								sess.lastUpdated = (new Date())
									.getTime();
								sess.markModified('token');
								sess.save();
							}
							else
							{
								var session = new Session(
								{
									userId: docs._id,
									token: token,
									lastUpdated: (new Date())
										.getTime()
								});
								session.save();
							}
							docs.password = "booksinstalove";
							//console.log("stored " + token);
							res.send(
							{
								token: token,
								doc: docs,
								valid: true
							});
						});
					}
					else
					{
						res.send(
						{
							valid: false
						});
					}
				}
				else
				{
					res.send(
					{
						valid: false
					});
				}*/
		});
	});
	app.get("/getUser", function(req, res)
	{
		User.findOne(
		{
			email: (req.query.email)
				.toLowerCase()
		}, function(err, doc)
		{
			res.header("Access-Control-Allow-Origin", "*");
			if (doc != null)
			{
				res.send(
				{
					status: true,
					doc: doc
				});
			}
			else
			{
				res.send(
				{
					status: false
				});
			}
		});
	});
	app.get("/count", function(req, res)
	{
		var x = (req.query.searchText)
			.split(" ");
		var patt = "(.*)";
		for (var i = 0; i < x.length; i++) patt += x[i] + "(.*)";
		Listing.count(
		{
			name:
			{
				$regex: patt,
				$options: "$i"
			}
		}, function(err, c)
		{
			res.send(c + " ");
			console.log(c);
		});
	});
	app.get("/getListing", function(req, res)
	{
		var response = [];
		Listing.findOne(
		{
			_id: req.query.id,
			approved: true
		}, function(err, doc)
		{
			if (err) console.log(err);
			if (doc != null)
			{
				doc.trendCount += 0.7;
				console.log(doc.trendCount);
				doc.save();
				response.push(
				{
					listing: doc
				});
				Page.findById(doc.pageId, function(err, page)
				{
					if (err) console.log(err);
					response.push(
					{
						page: page
					});
					Category.findOne(
					{
						title: 'CSE'
					}, function(err, doc)
					{
						if (err) console.log(err);
						if (doc)
						{
							doc.getAncestors(function(err, docs)
							{
								console.log(docs[1].title);
								response.push(
								{
									parentCat: docs
								});
								Category.findOne(
								{
									title: 'Root'
								}, function(err, doc)
								{
									if (err) console.log(err);
									if (doc)
									{
										doc.getChildrenTree(function(err, docs)
										{
											response.push(
											{
												categories: docs
											});
											res.send(response);
										});
									}
									else
									{
										res.send("No categories found");
									}
								});
							});
						}
						else
						{
							res.send("No categories found");
						}
					})
				});
			}
			else
			{
				res.send();
			}
		});
	});
	app.get("/admin", function(req, res)
	{
		res.render("./admin.html");
	});
	app.get("/getAdminData", function(req, res)
	{
		Admin.findOne(
		{}, function(err, doc)
		{
			if (err) console.log(err);
			else res.send(doc);
		});
	});
	app.get("/updateAdminData", function(req, res)
	{
		Admin.findOne(
		{}, function(err, doc)
		{
			console.log(doc);
			if (doc != null)
			{
				if (err) console.log(err);
				else
				{
					if (req.query.type == "quote")
					{
						doc.quote = req.query.quote;
						doc.save();
						console.log(doc);
						res.send(doc);
					}
					else if (req.query.type == "slider")
					{
						//Slider Stuff
						var x = {
							image: req.query.url,
							target: req.query.target
						};
						var y = req.query.position;
						if (y == "" || y == 'undefined') doc.slider.push(x);
						else
						{
							var z = doc.slider;
							z[y] = x;
							doc.slider = z;
						}
						doc.save();
						console.log(doc);
						res.send(doc);
					}
					else
					{
						var x = {
							image: req.query.cat_url,
							target: req.query.catTarget,
							name: req.query.catName
						};
						var y = req.query.cPosition;
						if (y == "" || y == 'undefined') doc.category.push(x);
						else
						{
							var z = doc.category;
							z[y] = x;
							doc.category = z;
						}
						doc.save();
						console.log(doc);
						res.send(doc);
					}
				}
			}
			else
			{
				var admin = new Admin(
				{
					quote: req.query.quote,
					slider: [
					{
						image: req.query.url,
						target: req.query.target
					}],
					categories: [
					{
						image: req.query.cat_url,
						target: req.query.catTarget,
						name: req.query.catName
					}]
				});
				admin.save();
			}
		});
	});
	app.get("/deleteSlider", function(req, res)
	{
		Admin.findOne(
		{}, function(err, doc)
		{
			var x = doc.slider;
			console.log(x.length);
			x.splice(req.query.index, 1);
			console.log(x.length);
			doc.slider = x;
			res.send(doc);
			doc.save();
		});
	});
	app.get('/sign_s3', function(req, res)
	{
		var file_name = (new Date())
			.getTime() + req.query.file_name;
		aws.config.update(
		{
			accessKeyId: AWS_ACCESS_KEY,
			secretAccessKey: AWS_SECRET_KEY
		});
		var s3 = new aws.S3();
		var s3_params = {
			Bucket: S3_BUCKET,
			Key: file_name,
			Expires: 60,
			ContentType: req.query.file_type,
			ACL: 'public-read'
		};
		s3.getSignedUrl('putObject', s3_params, function(err, data)
		{
			if (err)
			{
				console.log(err);
			}
			else
			{
				var return_data = {
					signed_request: data,
					url: 'https://' + S3_BUCKET + '.s3.amazonaws.com/' + file_name
				};
				res.write(JSON.stringify(return_data));
				res.end();
			}
		});
	});
	app.get("/radialSearch", function(req, res)
	{
		var x = req.query.radius;
		var lat = req.query.lat;
		var lon = req.query.lon;
		var z = (req.query.searchText)
			.split(" ");
		console.log(z);
		var patt = "(.*)";
		for (var i = 0; i < z.length; i++) patt += z[i] + "(.*)";
		Listing.find(
		{
			city: req.query.city,
			name:
			{
				$regex: patt,
				$options: "$i"
			},
			approved: true
		}, function(err, docs)
		{
			docs.forEach(function(doc)
			{
				User.findOne(
				{
					email: doc.soldBy
				}, function(err, u)
				{
					var y = distanc(lat, lon, doc.lat, doc.lon);
					if (y <= x + u.deliverRadius)
					{ //add this listing to response; declare response array upar;
						response.push(doc);
					}
				});
			});
			res.send(response);
		});
	});
	app.post("/obny", function(req, res)
	{
		console.log("Entering obny");
		var response = [];
		var a;
		var lat = req.query.lat;
		var lon = req.query.lon;
		Listing.count(
		{
			city: req.query.city
		}, function(err, c)
		{
			console.log(c);
			a = c;
			if (c == 0)
			{
				console.log("No result Found");
				res.send("No result Found");
			}
		});
		Listing.find(
		{
			city: req.query.city,
			approved: true
		}, function(err, docs)
		{ //add category,old filter also
			docs.forEach(function(doc)
			{
				var dis = distanc(parseFloat(lat), parseFloat(lon), doc.lat, doc.lon);
				if (dis <= 30)
				{
					console.log(lat + " " + lon + " " + doc.lat + " " + doc.lon + " " + dis);
					response.push(
					{
						'doc': doc
					});
				}
				a--;
				if (a == 0)
				{
					console.log("done");
					res.send(response);
				}
			});
		});
	});
	app.get("/sendLatLon", function(req, res)
	{
		var sent = false;
		var a;
		geocoder.geocode(req.query.pc, function(err, resp)
		{
			a = resp.length;
			console.log(a);
			resp.forEach(function(doc)
			{
				a--;
				if (doc.countryCode == "IN" && !sent)
				{
					sent = true;
					res.send(doc);
				}
				if (a == 0 && !sent) res.send("Not valid");
			});
		});
	});
	app.get("/printSeries", function(req, res)
	{
		if (req.query.n == null || req.query.n == 'undefined') res.send("?n=");
		else
		{
			var x = parseInt(req.query.n);
			var y = "";
			var z = 1;
			var a = 2;
			var i = 0;
			for (i = 0; i < x; i++)
			{
				y += z + "," + a + ",";
				z += 3;
				a += 3;
			}
			var resp = [];
			resp.push(
			{
				'skip 3rd': y
			});
			y = "";
			z = 1;
			for (i = 0; i < x; i++)
			{
				y += z + ",";
				z += 3;
			}
			var t = "",
				d = 2;
			for (i = 0; i < x; i++)
			{
				t += d + ",";
				d += 3;
			}
			console.log(t);
			resp.push(
			{
				'only 1st page': y
			});
			resp.push(
			{
				'only 2nd page': t
			});
			res.send(resp);
		}
	});
	app.get("/getTags", function(req, res)
	{
		var tag = req.query.tag;
		var patt = "(.*)";
		var x = tag.split(" ");
		for (var i = 0; i < x.length; i++) patt += x[i] + "(.*)";
		console.log(patt);
		Tags.find(
		{
			tag:
			{
				$regex: patt,
				$options: "$i"
			}
		}, function(err, docs)
		{
			if (err) console.log(err);
			console.log(docs);
			res.send(docs);
		});
	});
	app.get("/addCategory", function(req, res)
	{
		console.log("1");
		var name = req.query.name;
		var pname = req.query.pname;
		var image = req.query.image;
		console.log(name);
		console.log(pname);
		//console.log(name);
		if (pname == null || pname == 'undefined' || pname == '')
		{
			console.log("3");
			var category = new Category(
			{
				title: name,
				image: image
			});
			category.save(function(err, doc)
			{
				if (err) console.log(err);
				res.send(doc);
			});
		}
		else
		{
			console.log("2");
			var category = new Category(
			{
				title: name
			});
			Category.findOne(
			{
				title: pname
			}, function(err, doc)
			{
				console.log(doc);
				category.parent = doc;
				category.image = image;
				category.save(function(err, docs)
				{
					if (err)
						console.log(err);
					if (docs != null)
					{
						console.log(docs);
						res.send(docs);
					}
					else
					{
						console.log("docs null");
						res.send();
					}
				});
			});
		}
	});
	app.get("/getChildren", function(req, res)
	{
		var title = req.query.title;
		if (title == null || title == '' || title == 'undefined')
		{
			re.send();
		}
		else
		{
			Category.findOne(
			{
				title: title
			}, function(err, docs)
			{
				if (err) console.log(err);
				docs.getChildren(function(err, users)
				{
					res.send(users);
				});
			});
		}
	});
	app.get("/allCategories", function(req, res)
	{
		var args = {
			fields: "title children",
			recursive: true,
			allowEmptyChildren: false
		};
		Category.findOne(
		{
			title: 'Root'
		}, function(err, doc)
		{
			if (err) console.log(err);
			if (doc)
			{
				doc.getChildrenTree(function(err, docs)
				{
					res.send(docs);
				});
			}
			else
			{
				res.send("No categories found");
			}
		});
	});
	app.get("/getChildren", function(req, res)
	{
		var pname = req.query.pname;
		Category.findOne(
		{
			title: pname
		}, function(err, doc)
		{
			if (err) console.log(err);
			doc.getChildern(function(err, docs)
			{
				if (err) console.log(err);
				res.send(docs);
			});
		});
	});
	app.get("/allCourses", function(req, res)
	{
		Category.findOne(
		{
			title: 'Education'
		}, function(err, docs)
		{
			if (err) console.log(err);
			if (docs != null) docs.getChildren(true, function(err, users)
			{
				if (err) console.log(err);
				res.send(users);
			});
		});
	});
	app.get("/getPages", function(req, res)
	{
		var data = req.query.data;
		var type = req.query.type;
		if (type == 1)
		{
			var tagss = data.split(" ");
			var iTags = [];
			tagss.forEach(function(tag)
			{
				var re = new RegExp(tag, 'i');
				iTags.push(re);
			});
			Page.find(
			{
				tags:
				{
					$in: iTags
				},
				approved: true
			}, function(err, docs)
			{
				if (err) console.log(err);
				res.send(docs);
			});
		}
		else if (type == 2)
		{
			var x = data.split(" ");
			var patt = "(.*)";
			for (var i = 0; i < x.length; i++) patt += x[i] + "(.*)";
			Page.find(
			{
				title:
				{
					$regex: patt,
					$options: "$i"
				},
				approved: true
			}, function(err, docs)
			{
				if (err) console.log(err);
				res.send(docs);
			});
		}
		else if (type == 3)
		{
			var x = data.split(" ");
			var patt = "(.*)";
			for (var i = 0; i < x.length; i++) patt += x[i] + "(.*)";
			Page.find(
			{
				author:
				{
					$regex: patt,
					$options: "$i"
				},
				approved: true
			}, function(err, docs)
			{
				if (err) console.log(err);
				res.send(docs);
			});
		}
	});
	app.get("/checkPageId", function(req, res)
	{
		var id = req.query.id;
		Page.count(
		{
			_id: id
		}, function(err, c)
		{
			console.log(c);
			if (c == 1) res.send(
			{
				status: true
			});
			else res.send(
			{
				status: false
			});
		});
	});
	app.get("/recentUploads", function(req, res)
	{
		var response = [];
		var pages = [];
		var a;
		Listing.count(
			{}, function(err, c)
			{
				if (err) console.log(err);
				console.log(c);
				a = c;
			})
			.limit(10);
		Listing.find(
			{
				approved: true
			}, function(err, docs)
			{
				if (err) console.log(err);
				else
				{
					response.push(
					{
						listings: docs
					});
					docs.forEach(function(listing)
					{
						console.log(a);
						Page.findById(listing.pageId, function(err, page)
						{
							a--;
							pages.push(page);
							if (a == 0)
							{
								console.log(pages);
								response.push(
								{
									pagess: pages
								});
								res.send(response);
							}
						});
					});
				}
			})
			.limit(10);
	});
	app.get("/pendingPages", function(req, res)
	{
		var skip = req.query.skip;
		var limit = req.query.limit;
		Page.find(
			{
				approved: false
			}, function(err, docs)
			{
				if (err) console.log(err);
				res.send(docs);
			})
			.skip(skip)
			.limit(limit);
	});
	app.get("/approvePage", function(req, res)
	{
		var id = req.query.id;
		Page.findById(id, function(err, doc)
		{
			if (err) console.log(err);
			doc.approved = true;
			doc.save();
			res.send(doc);
		});
	});
	app.get("/approveListing", function(req, res)
	{
		var id = req.query.id;
		Listing.findById(id, function(err, doc)
		{
			if (err) console.log(err);
			doc.approved = true;
			doc.save();
			res.send(doc);
		});
	});
	app.get("/trending", function(req, res)
	{
		var skip = 0;
		var response = '';
		var interests = [];
		var interestList = [];
		var trending = [];
		var ru = [];
		var obny = [];
		var a = false;
		var b = false;
		var c = false;
		var d = false;

		function sendResponse()
		{
			if (a && b && c && d)
			{
				res.json(
				{
					trending: trending,
					ru: ru,
					obny: obny
				});
				//				res.send(JSON.parse(response));
			}
		};
		//for Trending
		Listing.find(
			{
				approved: true
			}, function(err, docs)
			{
				if (err) console.log(err);
				var x = docs.length;
				docs.forEach(function(listing)
				{
					Page.findById(
					{
						_id: listing.pageId
					}, function(err, page)
					{
						if (err) console.log(err);
						trending.push(
						{
							listing: listing,
							page: page
						});
						x--;
						if (x == 0 || x == -1)
						{
							/*response.push(
							{
								type: 'Trending',
								docs: trending
							});*/
							a = true;
							console.log("A");
							sendResponse();
						}
					});
				});
			})
			.sort('-trendCount')
			.skip(0)
			.limit(2);
		//For Recent Uploads
		Listing.find(
			{
				approved: true
			}, function(err, docs)
			{
				if (err) console.log(err);
				var x = docs.length;
				docs.forEach(function(listing)
				{
					Page.findById(
					{
						_id: listing.pageId
					}, function(err, page)
					{
						if (err) console.log(err);
						ru.push(
						{
							listing: listing,
							page: page
						});
						x--;
						if (x == 0 || x == -1)
						{
							/*response.push(
							{
								type: 'ru',
								docs: ru
							});*/
							b = true;
							console.log("b");
							sendResponse();
						}
					});
				});
			})
			.skip(skip)
			.limit(2);
		console.log(response);
		//For OBNY
		Listing.find(
			{
				city: req.query.city,
				approved: true
			}, function(err, docs)
			{
				if (err) console.log(err);
				console.log("here");
				if (docs != null)
				{
					console.log("here2");
					var x = docs.length;
					console.log(x);
					if (x == 0)
					{
						c = true;
						sendResponse();
					}
					docs.forEach(function(listing)
					{
						Page.findById(listing.pageId, function(err, page)
						{
							if (err) console.log(err);
							obny.push(
							{
								listing: listing,
								page: page
							});
							x--;
							if (x == 0 || x == -1)
							{
								/*response.push(
								{
									type: 'obny',
									docs: obny
								});*/
								c = true;
								console.log("c");
								sendResponse();
							}
						});
					});
				}
				else
				{
					c = true;
					console.log("c2");
					sendResponse();
				}
			})
			.skip(skip)
			.limit(2);
		//Trending in interests category of User
		if (req.query.email != null)
		{
			User.findOne(
			{
				email: req.query.email
			}, function(err, doc)
			{
				interests = doc.interests;
			});
			var x = interests.length;
			interests.forEach(function(interest)
			{
				Listing.find(
					{
						approved: true,
						category: interest
					}, function(err, docs)
					{
						if (err) console.log(err);
						var y = [];
						var z = docs.length;
						docs.forEach(function(listing)
						{
							Page.findById(listing.pageId, function(err, page)
							{
								if (err) console.log(err);
								y.push(
								{
									listing: listing,
									page: page
								});
								z--;
								if (z == 0 || z == -1)
								{
									d = true;
									console.log("d");
									interestList.push(
									{
										type: interest,
										docs: y
									});
								}
							});
						});
					})
					.sort('-trendCount')
					.skip(skip)
					.limit(2)
					.then(function(req, res) {});
				x--;
				if (x == 0 || x == -1)
				{
					/*response.push(
					{
						type: 'interests',
						docs: interestList
					});*/
					d = true;
					console.log("d2");
					sendResponse();
				}
			});
		}
		else
		{ //add trending from every category
			d = true;
			console.log("d3");
			sendResponse();
		}
	});
	app.get("/pendingListing", function(req, res)
	{
		var skip = req.query.skip;
		var response = [];
		var limit = req.query.limit;
		Listing.count(
		{
			approved: false
		}, function(err, c)
		{
			if (err) console.log(err);
			if (c == 0)
			{
				res.send();
			}
			else
			{
				Listing.find(
					{
						approved: false
					}, function(err, docs)
					{
						if (err) console.log(err);
						if (docs)
						{
							var x = docs.length;
							docs.forEach(function(doc)
							{
								Page.findById(doc.pageId, function(err, page)
								{
									response.push(
									{
										page: page,
										listing: doc
									});
									x--;
									console.log(x + " p");
									if (x == 0 || x == -1) res.send(response);
								});
							});
						}
					})
					.skip(skip);
			}
		});
	});
	app.get("/updateTrendCount", function(req, res)
	{
		var id = req.query.id;
		var count = req.query.count;
		var email = req.query.email;
		console.log(id);
		console.log(count);
		Listing.findById(id, function(err, doc)
		{
			if (err) console.log(err);
			if (doc)
			{
				console.log("before :" + doc.trendCount);
				doc.trendCount += parseInt(count);
				doc.save();
				console.log("after :" + doc.trendCount);
				console.log(email);
				User.findOne(
				{
					email: email
				}, function(err, docs)
				{
					if (err) console.log(err);
					var index = docs.booksLiked.indexOf(id);
					if (index == -1)
					{
						if (count == 1) docs.booksLiked.push(id);
						else docs.booksLiked.splice(index, 1);
						docs.markModified('booksLiked');
						console.log(docs);
						docs.save();
						res.send(docs);
					}
					else
					{
						res.send(docs);
					}
				});
			}
		});
	});
	app.get("/updateDP", function(req, res)
	{
		User.findOne(
		{
			email: req.query.email
		}, function(err, doc)
		{
			if (err) console.log(err);
			else
			{
				doc.dp = req.query.url;
				doc.save(function(err, docs)
				{
					if (err) console.log(err);
					else
					{
						docs.password = "******";
						res.send(docs);
					}
				});
			}
		});
	});
	app.get("/deleteListing", function(req, res)
	{
		var id = req.query.id;
		Listing.findById(id, function(err, doc)
		{
			if (err) console.log(err);
			else if (doc != null)
			{
				doc.remove();
				res.send("Delete Successful");
			}
			else
			{
				res.send("check id");
			}
		});
	});
	app.get("/addReview", function(req, res)
	{
		var listingId = req.query.id;
		var review = req.query.review;
		var rEmail = req.query.email;
		var rDp = req.query.rDP;
		var rName = req.query.rName;
		Listing.findById(listingId, function(err, listing)
		{
			if (err)
				console.log(err);
			if (listing != null)
			{
				var reviewData = '{"review":"' + review + '","rEmail":"' + rEmail + '","rDp":"' + rDp + '","rName":"' + rName + '"}';
				var obj = JSON.parse(reviewData);
				listing.reviews.push(obj);
				listing.markModified('reviews');
				listing.save(function(err, doc)
				{
					if (err)
						console.log(err);
					if (doc != null)
						res.send(doc);
				});
			}
		});
	});
	app.get("/test2", function(req, res)
	{
		//res.render("./free.html");
	});
	app.get("/ltu", function(req, res) //ltu = listingthrough username
		{
			var username = req.query.username;
			User.findOne(
			{
				username: username
			}, function(err, user)
			{
				if (err) console.log(err);
				if (user == null)
				{
					res.send("Error 404 -franbrain.com");
				}
				else
				{
					console.log("here");
					console.log(user);
					var userListings = [];
					var listingPages = [];
					var listingIds = user.listingIds;
					var x = listingIds.length;
					console.log(x);
					listingIds.forEach(function(id)
					{
						Listing.findById(id, function(err, listing)
						{
							if (err) console.log(err);
							if (listing != null)
							{
								Page.findById(listing.pageId, function(err, page)
								{
									if (err) console.log(err);
									if (page != null)
									{
										userListings.push(listing);
										listingPages.push(page);
										x--;
										console.log(x);
										if (x == 0) res.send(
										{
											listing: userListings,
											page: listingPages
										});
									}
									else
									{
										x--;
										console.log(x);
										if (x == 0) res.send(
										{
											listing: userListings,
											page: listingPages
										});
									}
								});
							}
							else
							{
								x--;
								console.log(x);
								if (x == 0) res.send(
								{
									listing: userListings,
									page: listingPages
								});
							}
						});
					});
				}
			});
		});
	app.get("/*", function(req, res)
	{
		res.send("kithe???");
	});
	return app;
};
