var mongoose = require("mongoose");
var userSchema = {
    name: {
       // // required: true,
        type: String
    },
    email: {
        type: String,
        // required: true,
        unique: true,
        sparse: true
    },
    cno: {
        // required: true,
        type: Number
    },
    hash: String,
    salt: String,
    course: {
        type: String
    },
    lat: {
        type: Number,
        // required: true
    },
    lon: {
        type: Number,
        // required: true
    },
    city: {
        type: String,
        // required: true
    },
    listingIds: [{
        type: mongoose.Schema.ObjectId,
        ref: 'Listing'
    }],
    interests: [{
        type: String
    }],
    dp: {
        type: String
    },
    radius: {
        type: Number,
        default: 0,
        // required: true
    },
    booksLiked: [{
        type: mongoose.Schema.ObjectId,
        ref: 'Listing'
    }],
    collectionBooks: [{
        type: mongoose.Schema.ObjectId,
        ref: 'Listing'
    }],
    sellerType: { //vendor = true
        type: Boolean
    },
    username: {
        type: String,
        //unique: true
    },
    loginProvider:[{type:String}],
    providerid:[{type:String}],
    providerToken:[{type:String
    }],
    providerUsername:[{type:String
    }]

};
var schema = new mongoose.Schema(userSchema);
module.exports = schema;
module.exports.userSchema = schema;