var mongoose = require("mongoose");
var pageSchema = {
    title:
	{
	type:String,
	required:true},
	author:
	{
	type:String,
	required:true},
	tags :[],
	listings:[{//bad m
	type:mongoose.Schema.ObjectId,
	ref:'Listing'
	}],
	reviews:[{type:String}],//bad m
	coverImage:{//bad m
	type:String,
	},
	description:{
	type:String},
	category:{
	type:String,
	required:true},
	approved:{
	type:Boolean,
	default:false},
	ISBN:{
	type:Number}
	
};
var schema = new mongoose.Schema(pageSchema);
module.exports = schema;
module.exports.pageSchema = schema;