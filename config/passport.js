var LocalStrategy = require('passport-local')
    .Strategy;
var FacebookStrategy = require('passport-facebook')
    .Strategy;
var GoogleStrategy = require('passport-google-oauth')
    .OAuth2Strategy;
var InstagramStrategy = require('passport-instagram')
    .Strategy;
var GoodreadsStrategy = require('passport-goodreads')
    .Strategy;
var TwitterStrategy = require('passport-twitter')
    .Strategy;
var configAuth = require('./auth');
module.exports = function(passport, User,app)
{
    passport.serializeUser(function(user, done)
    {
        done(null, user.id);
    });
    // used to deserialize the user
    passport.deserializeUser(function(id, done)
    {
        User.findById(id, function(err, user)
        {
            done(err, user);
        });
    });
    passport.use(new TwitterStrategy(
        {
            consumerKey: configAuth.twitterAuth.clientID,
            consumerSecret: configAuth.twitterAuth.clientSecret,
            callbackURL: "http://localhost:8888/auth/twitter/callback"
        },
        function(token, tokenSecret, profile, done)
        {
            console.log("1 0",profile);
             User.findOne(
                {
                    'providerid': profile.id
                }, function(err, user)
                {
                    if (err)
                        return done(err);
                    if (user)
                    {    
                        user.save(function(err)
                        {
                                if (err)
                                throw err;
                            // if successful, return the new user
                            return done(null, user);
                        
                        });
                        //return done(null, user); // user found, return that user
                    }
                    else
                    {
                        // if the user isnt in our database, create a new user
               
                        var newUser = new User();
                        newUser.loginProvider.push('twitter');
                        newUser.providerid.push(profile.id);
                        newUser.providerToken.push(token);
                        newUser.providerUsername.push(profile.username);
                        newUser.name = profile.displayName;
                        newUser.dp= profile.photos[0].value;
                      
                        // save the user
                        newUser.save(function(err,doc)
                        {
                            if (err)
                                throw err;
                            console.log(doc);
                            return done(null, doc);
                        });  



                    }
                });
        
        }));
    passport.use(new GoodreadsStrategy(
        {
            consumerKey: configAuth.goodReadsAuth.clientID,
            consumerSecret: configAuth.goodReadsAuth.clientSecret,
            callbackURL: "http://localhost:8888/auth/goodreads/callback"
        },
        function(token, tokenSecret, profile, done)
        {
            // asynchronous verification, for effect...
            process.nextTick(function()
            {
             User.findOne(
                {
                    'providerid': profile.id
                }, function(err, user)
                {
                    if (err)
                        return done(err);
                     if (user)
                    {    
                        user.save(function(err)
                        {
                                if (err)
                                throw err;
                            // if successful, return the new user
                            return done(null, user);
                        
                        });
                        //return done(null, user); // user found, return that user
                    }
                    else
                    { 
                        console.log(profile);
                       var newUser = new User();
                        newUser.loginProvider.push('goodreads');
                        newUser.providerid.push(profile.id);
                        newUser.providerToken.push(token);
                        newUser.name = profile.displayName;
                        newUser.dp="http://d.gr-assets.com/misc/1454549160-1454549160_goodreads_misc.png";
                      
                        // save the user
                        newUser.save(function(err)
                        {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        }); 
                    




                    }
                });});
        }
    ));
    passport.use(new InstagramStrategy(
        {
            clientID: configAuth.instaAuth.clientID,
            clientSecret: configAuth.instaAuth.clientSecret,
            callbackURL: "http://localhost:8888/auth/instagram/callback"
        },
        function(token, refreshToken, profile, done)
        {
            // asynchronous verification, for effect...
            process.nextTick(function()
            {
                User.findOne(
                {
                    'providerid': profile.id
                }, function(err, user)
                {
                    if (err)
                        return done(err);
                     if (user)
                    {    
                        user.save(function(err)
                        {
                                if (err)
                                throw err;
                            // if successful, return the new user
                            return done(null, user);
                        
                        });
                        //return done(null, user); // user found, return that user
                    }
                    else
                    {
                        console.log(profile);
                          var newUser = new User();
                        newUser.loginProvider.push('instagram');
                        newUser.providerid.push(profile.id);
                        newUser.providerToken.push(token);
                        newUser.name = profile.displayName;
                        newUser.providerUsername.push(profile.username);
                        newUser.dp= profile._json.data.profile_picture;
                      
                        // save the user
                        newUser.save(function(err)
                        {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });    
                    



                    }
                });
            });
        }
    ));
    passport.use(new GoogleStrategy(
        {
            clientID: configAuth.googleAuth.clientID,
            clientSecret: configAuth.googleAuth.clientSecret,
            callbackURL: "http://localhost:8888/auth/google/callback",
        },
        function(token, refreshToken, profile, done)
        {
            // make the code asynchronous
            // User.findOne won't fire until we have all our data back from Google
            process.nextTick(function()
            {
                // try to find the user based on their google id
                User.findOne(
                {
                    'providerid': profile.id
                }, function(err, user)
                {
                    if (err)
                        return done(err);
                    if (user)
                    {    
                        user.save(function(err)
                        {
                                if (err)
                                throw err;
                            // if successful, return the new user
                            return done(null, user);
                        
                        });
                        //return done(null, user); // user found, return that user
                    }
                    else
                    {
                        // if the user isnt in our database, create a new user
                        User.findOne({email:profile.emails[0].value},function(err,doc){
                            if(err)
                            {
                            console.log(err);
                            throw err;
                            return done(null,doc);
                            }
                            else
                            {
                                if(doc==null)
                                {

                        var newUser = new User();
                        newUser.email = profile.emails[0].value; // pull the first email
                        newUser.loginProvider.push('google');
                        newUser.providerid.push(profile.id);
                        newUser.providerToken.push(token);
                        newUser.name = profile.displayName;
                        newUser.dp= profile.photos[0].value;
                      
                        // save the user
                        newUser.save(function(err)
                        {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });         
                                }
                                else if(doc!=null)
                                {


                        doc.loginProvider.push('google');
                        doc.providerid.push(profile.id);
                        doc.providerToken.push(token);
                        if(doc.dp)
                        {

                        }
                        else
                        doc.dp=profile.photos[0].value;                      
                        // save the user
                        doc.save(function(err)
                        {
                            if (err)
                                throw err;
                            return done(null, doc);
                        });

                                }

                            }
                        });
                    }
                });
            });
        }));
    passport.use(new FacebookStrategy(
        {
            // pull in our app id and secret from our auth.js file
            clientID: configAuth.facebookAuth.clientID,
            clientSecret: configAuth.facebookAuth.clientSecret,
            callbackURL: "http://localhost:8888/auth/facebook/callback",
            profileFields: ["emails", "displayName"]
        },
        // facebook will send back the token and profile
        function(token, refreshToken, profile, done)
        {
                // asynchronousx
            process.nextTick(function()
            {
                console.log("here");
                // find the user in the database based on their facebook id
                User.findOne(
                {
                    'providerid': profile.id
                }, function(err, user)
                {
                    // if there is an error, stop everything and return that
                    // ie an error connecting to the database
                    if (err)
                        return done(err);
                    // if the user is found, then log them in
                    if (user)
                    {    
                        

                            // if successful, return the new user
                            return done(null, user);
                        
                        
                        //return done(null, user); // user found, return that user
                    }
                    else
                    {
                        // if the user isnt in our database, create a new user
                        User.findOne({email:profile.emails[0].value},function(err,doc){
                            if(err)
                            {
                            console.log(err);
                            throw err;
                            return done(null,doc);
                            }
                            else
                            {
                                if(doc==null)
                                {

                        var newUser = new User();
                        newUser.email = profile.emails[0].value; // pull the first email
                        newUser.loginProvider.push('facebook');
                        newUser.providerid.push(profile.id);
                        newUser.providerToken.push(token);
                        newUser.name = profile.displayName;
                        newUser.dp= "http://graph.facebook.com/"+profile.id+"/picture?type=large";
                      
                        // save the user
                        newUser.save(function(err)
                        {
                            if (err)
                                throw err;
                            return done(null, "abrakadabra");
                        });         
                                }
                                else if(doc!=null)
                                {


                        doc.loginProvider.push('facebook');

                        doc.providerid.push(profile.id);
                        
                        doc.providerToken.push(token);
                        
                        doc.dp= "http://graph.facebook.com/"+profile.id+"/picture?type=large";
                      
                        // save the user
                        doc.save(function(err)
                        {
                            if (err)
                                throw err;
                            return done(null, "abrakadabra");
                        });

                                }

                            }
                        });
                    






                    }
                });
            });
        }));
    passport.use(new LocalStrategy(
        {
            usernameField: 'email'
        },
        function(username, password, done)
        {
            User.findOne(
            {
                email: username
            }, function(err, user)
            {
                if (err)
                {
                    return done(err);
                }
                // Return if user not found in database
                if (!user)
                {
                    return done(null, false,
                    {
                        message: 'User not found'
                    });
                }
                // Return if password is wrong
                if (!user.validPassword(password))
                {
                    return done(null, false,
                    {
                        message: 'Password is wrong'
                    });
                }
                // If credentials are correct, return the user object
                return done(null, user);
            });
        }
    ));
};
