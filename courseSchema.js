var mongoose = require("mongoose");
var courseSchema = {
	name : {
		required : true,
		type : String
	}
};
var schema = new mongoose.Schema(courseSchema);
module.exports = schema;
module.exports.courseSchema = schema;