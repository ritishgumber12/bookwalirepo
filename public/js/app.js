	var app = angular.module("booksApp", ['ngCookies', 'ngSanitize', 'angular-flexslider']);
	app.controller("registerCtrl", function($scope, $http, $window)
	{
		 var saveToken = function (token) {
      $window.localStorage['mean-token'] = token;
    };

   
   
		$scope.SendData=function()
		{
			$http.post("/addUser?uname="+$scope.uname+"&&cno="+$scope.cno+"&&username="+$scope.username+"&&email="+$scope.email+"&&pass="+$scope.pass).success(function(response){
			if(response.success)
			{
			saveToken(response.token);
			$window.location.href = '/';
			}
			});
		}
		
		$scope.getPinCode = function()
		{
			$http.get("/sendLatLon?pc=" + $scope.uaddr)
				.then(function(res)
				{
					console.log(res.data);
					$scope.pincode = res.data.zipcode;
					$scope.lat = res.data.latitude;
					$scope.lon = res.data.longitude;
				});
		};
		$scope.checkEmail = function()
		{
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (re.test($scope.email))
			{
				$http.get("/checkEmail?email=" + $scope.email)
					.then(function(res)
					{
						if (!res.data.status)
						{
							setwarning('basicdetailswarningtext', 'Email already in use!');
							window.mailtrue = 0;
						}
						else
						{
							hidewarning('basicdetailswarningtext');
							window.mailtrue = 1;
						}
					});
			}
			else
			{
				setwarning('basicdetailswarningtext', 'Email Id not valid!');
			}
		};
		$scope.checkUsername = function()
		{
			var usernameRegex = /^[a-zA-Z0-9]+$/;
			if (usernameRegex.test($scope.username))
			{
				$http.get("/checkUsername?username=" + $scope.username)
					.then(function(res)
					{
						if (res.data.status)
						{
							validateusername(true);
						}
						else
						{
							validateusername(false);
						}
					});
			}
			else
			{
				validateusername(false);
			}
		};
	});
	app.controller("productCtrl", function($scope, $http, $location, $sce, $timeout, $cookies)
	{
		var x;
		console.log($location);
		$scope.appendData = "";
		var id = $location.$$search.id;
		console.log(id);
		$scope.loggedIn = false;
		if ($cookies.get('lt') != null)
		{
			$http.post('/api/authenticate?lt=' + $cookies.get('lt'))
				.then(function(res)
				{
					if (res.data.valid)
					{
						$scope.loggedIn = true;
						$scope.loginUrl = '/profile#?email=' + res.data.doc.email;
						$scope.loginSpan = res.data.doc.name;
						$scope.userData = res.data.doc;
						console.log($scope.userData);
					}
					else
						$cookies.remove('lt');
				});
		}
		$http.get("/getListing?id=" + $locationlocation.$$search.id)
			.then(function(response)
			{
				//	x = response.data;
				console.log(response.data);
				$scope.listing = response.data[0].listing;
				$scope.page = response.data[1].page;
				$scope.childrenn = response.data[3].categories;
				$scope.listingParentCat = [];
				var x = response.data[2].parentCat;
				x.forEach(function(cat)
				{
					$scope.listingParentCat.push(cat.title);
					console.log($scope.listingParentCat)
				});
				console.log($scope.listing);
				console.log($scope.page);
				/*var z = "";
			for (var i = 0; i < $scope.listing.images.length; i++)
			{
				z += '<li data-thumb="' + $scope.listing.images[i] + '"><img class="maximg" data-src="' + $scope.listing.images[i] + '" /></li>'
				$scope.appendData = $sce.trustAsHtml(z);
				/*console.log(i);
				console.log(z);	
				oa();
			}
			    $timeout(oa, 0);
 */
				$http.get("/getUser?email=" + $scope.listing.soldBy)
					.then(function(response)
					{
						console.log("sellerDa");
						$scope.sellerData = response.data.doc;
						console.log($scope.sellerData);
					});
			});
		$scope.sendReview = function()
		{
			if ($scope.loggedIn)
			{
				$http.get("/addReview?id=" + $scope.listing._id + "&review=" + $scope.review + "&email=" + $scope.userData.email + "&rDP=" + $scope.userData.dp + "&rName=" + $scope.userData.name)
					.then(function(res)
					{
						$scope.listing = res.data;
						console.log($scope.listing.reviews);
					});
			}
			else
			{
				$('#modal1')
					.modal(
					{
						show: true,
						backdrop: 'static',
						keyboard: false
					});
			}
		};
		$scope.checkUser = function(email, pass)
		{
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (re.test(email))
			{
				$http.get("/checkUser?email=" + email + "&&password=" + pass)
					.then(function(response)
					{
						$scope.wrongPass = !response.data.valid;
						if (!$scope.wrongPass)
						{
							$scope.loggedIn = true;
							$scope.loginUrl = "/profile#?email=" + email;
							$scope.loginSpan = response.data.doc.name;
							$cookies.put("lt", response.data.token);
							$scope.email = response.data.doc.email;
							$scope.userData = response.data.doc;
							console.log($scope.userData);
							$('#modal1')
								.modal('hide');
						}
					});
			}
			else
			{
			}
		};
	});
	app.controller("adminCtrl", function($scope, $http, $rootScope)
	{
		$scope.deleteListing = function()
		{
			$http.get("/deleteListing?id=" + $scope.listingid)
				.then(function(res)
				{
					console.log(res.data);
				});
		}
		$scope.skip = 0;
		$http.get("/pendingPages?skip=" + $scope.skip + "&limit=10")
			.then(function(res)
			{
				console.log(res.data);
				$scope.pendingPages = res.data;
			});
		$http.get("/pendingListing?skip=" + $scope.skip + "&limit=10")
			.then(function(res)
			{
				console.log(res.data);
				$scope.pendingListings = res.data;
			});
		$scope.saveCoverImage = function(obj, a)
		{
			if (obj.which == 13)
			{
				$http.get("/updateCoverImage?id=" + obj.target.id + "&url=" + a)
					.then(function(res)
					{
						$http.get("/pendingPages?skip=" + $scope.skip + "&limit=10")
							.then(function(res)
							{
								console.log(res.data);
								$scope.pendingPages = res.data;
							});
					});
			}
		};
		$scope.approvePage = function(obj)
		{
			var id = obj.target.attributes.id.value;
			$http.get("/approvePage?id=" + id)
				.then(function(res)
				{
					$http.get("/pendingPages?skip=" + $scope.skip + "&limit=10")
						.then(function(res)
						{
							console.log(res.data);
							$scope.pendingPages = res.data;
						});
				});
		};
		$scope.approveListing = function(obj)
		{
			var id = obj.target.attributes.id.value;
			$http.get("/approveListing?id=" + id)
				.then(function(res)
				{
					$http.get("/pendingListing?skip=" + $scope.skip + "&limit=10")
						.then(function(res)
						{
							console.log(res.data);
							$scope.pendingListings = res.data;
						});
				});
		};
		$scope.deleteTag = function(obj, index, type)
		{
			var data = obj.target.attributes.id.value;
			var x = data.split("$$");
			var tag = x[0];
			var id = x[1];
			console.log(tag);
			console.log(id);
			console.log(index);
			$http.get("/deleteTag?tag=" + tag + "&id=" + id + "&index=" + index + "&type=" + type)
				.then(function(res)
				{
					$http.get("/pendingPages?skip=" + $scope.skip + "&limit=10")
						.then(function(res)
						{
							console.log(res.data);
							$scope.pendingPages = res.data;
						});
				});
		};
		$scope.deleteSlider = function()
		{
			console.log("lmda");
			$http.get("/deleteSlider?index=" + $scope.sliderIndex)
				.then(function(response)
				{
					$scope.adminData = response.data;
				});
		};
		$http.get("/getAdminData")
			.then(function(response)
			{
				$scope.adminData = response.data;
				console.log($scope.adminData);
			});
		$scope.updateAdminData = function(a)
		{
			$http.get("/updateAdminData?quote=" + $scope.quote + "&type=" + a + "&url=" + $scope.slider_url + "&position=" + $scope.position + "&target=" + $scope.target + "&catTarget=" + $scope.catTarget + "&cat_url=" + $scope.cat_url + "&cPosition=" + $scope.cPosition + "&catName=" + $scope.catName)
				.
			then(function(response)
			{
				console.log(response.data);
				$scope.adminData = response.data;
			});
		};
		$scope.a = function()
		{
			var ele = angular.element('#slider_url');
			var ulwidth = ele[0].clientWidth;
		};
	});
	app.controller("testCtrl", function($scope, $http, $rootScope)
	{
		$scope.$on('eventName', function(event, args)
		{
			$scope.color = 'red';
			console.log($scope.color);
		});
		$scope.call = function(x)
		{
			console.log("called");
			console.log(x);
		};
	});
	app.controller("userProfileCtrl", function($scope, $http, $window, $cookies, $location)
	{
		var email = $location.$$search.email;
		console.log(email);
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		console.log(re.test(email));
		$scope.updateDP = function(x)
		{
			$http.get("/updateDP?email=" + email + "&url=" + x)
				.then(function(res)
				{
					console.log(res.data);
				});
		};
		if (re.test(email))
		{
			$http.get("/getUser?email=" + email)
				.then(function(response)
				{
					if (response.data.status)
					{
						$scope.userData = response.data.doc;
						console.log(response.data.doc);
						$cookies.put("user", email);
					}
					else
					{
						$window.location.href = '/register';
					}
				});
		}
		else
		{
			$window.location.href = '/register';
		}
		$scope.pageSize = 10;
		var text;
		$scope.addButtons = function()
		{
			if (($scope.page + 5) * $scope.pageSize < $scope.count) $scope.page += 5;
			text = $scope.searchText;
		};
		$scope.subButtons = function()
		{
			$scope.page -= 5;
			text = $scope.searchText;
		};
		$scope.getBooks = function(obj)
		{
			//console.log("dd");
			if (text != $scope.searchText) $scope.page = 0;
			if (obj != null) var sk = obj.target.attributes.id.value;
			else sk = 1;
			console.log(sk);
			$http.get("/allE?searchText=" + $scope.searchText + "&&skip=" + (sk - 1) * $scope.pageSize + "&&pageSize=" + $scope.pageSize + "&&email=" + $cookies.get("user"))
				.then(function(response)
				{
					$scope.myWelcome = response.data;
				});
			$http.get("/countE?searchText=" + $scope.searchText + "&&email=" + $cookies.get("user"))
				.then(function(response)
				{
					$scope.count = response.data;
				});
		};
		$scope.searchText = "";
		$http.get("/allE?searchText=&&email=" + $cookies.get("user") + "&&skip=0&&pageSize=" + $scope.pageSize)
			.then(function(response)
			{
				$scope.myWelcome = response.data;
			});;
	});
	app.directive('count', function()
	{
		return {
			templateUrl: '/count?searchText=' + document.getElementById('searchText')
				.value
		};
	});
	app.filter('regex', function()
	{
		return function(input, field, s)
		{
			var s = s.split(" ");
			//console.log(s);
			var x = "(.*)"
			for (var j = 0; j < s.length; j++)
			{
				x += (s[j] + "(.*)");
			}
			var patt = new RegExp(x, 'i');
			//console.log(patt);
			var out = [];
			for (var i = 0; i < input.length - 1; i++)
			{
				if (patt.test(input[i][field])) out.push(input[i]);
			}
			return out;
		};
	});
	app.filter('range', function()
	{
		return function(input, total, page, pageSize)
		{
			total = parseInt(total);
			if (total > 5) total = page + 4;
			for (var i = page; i <= total; i++)
			{
				input.push(i + 1);
			}
			return input;
		};
	});
	app.controller("addCatCtrl", function($scope, $http)
	{
		$http.get("/allCategories")
			.then(function(response)
			{
				console.log(response.data);
				$scope.childrenn = response.data;
			});
		$scope.addCategory = function()
		{
			console.log("calling");
			$http.get("/addCategory?name=" + $scope.catName + "&pname=" + $scope.pname + "&image=" + $scope.image)
				.then(function(response)
				{
					console.log(response.data);
					$http.get("/test")
						.then(function(response)
						{
							console.log(response.data);
							$scope.childrenn = response.data;
						});
				});
		};
		$scope.showChildren = function(name)
		{
			console.log("Calling children");
			console.log(name);
			$http.get("/getChildren?title=" + name)
				.then(function(response)
				{
					console.log(response.data);
				});
		}
	});
	app.controller("addNewBookCtrl", function($scope, $http, $cookies, $window, $location)
	{$http.get("https://api.ote-godaddy.com/v1/domains/available?domain=npmjs.org&checkType=FAST&forTransfer=false&").then(function(res){
	console.log("test");
		console.log(res.data);
		});
		$scope.loggedIn = false;
		if ($cookies.get('lt') != null)
		{
			$http.post('/api/authenticate?lt=' + $cookies.get('lt'))
				.then(function(res)
				{
					if (res.data.valid)
					{
						$scope.loggedIn = true;
						$scope.loginUrl = '/profile#?email=' + res.data.doc.email;
						$scope.loginSpan = res.data.doc.name;
						$scope.email = res.data.doc.email;
						$('#modal1')
							.modal('hide');
					}
					else
						$cookies.remove('lt');
				});
		}
		else
		{
			$('#modal1')
				.modal(
				{
					show: true,
					backdrop: 'static',
					keyboard: false
				})
		}
		var pageId = $location.$$search.pageId;
		$scope.pageId = pageId;
		console.log(pageId);
		$http.get("/checkPageId?id=" + pageId)
			.then(function(res)
			{
				if (res.data.status)
				{
					$scope.status = true;
				}
				else
				{
					$scope.status = false;
				}
			});
		$scope.lat = $cookies.get("lat");
		$scope.lon = $cookies.get("lon");
		$scope.city = $cookies.get("city");
		$scope.tags = [];
		//$scope.cat2="bfkjsna,mcn,z";
		$http.get("/allCategories")
			.then(function(response)
			{
				console.log(response.data);
				$scope.categories = response.data;
				//$scope.cat2=$scope.categories[0];
				console.log($scope.categories);
			});
		var x, y;
		$scope.categoryy="";
		$scope.$watch('cat1', function(newValue, oldValue, scope)
		{
			$scope.cat2 = $scope.categories[newValue];
			x = newValue;
			$scope.cat3 = null;
			console.log(x);
		});
		$scope.$watch('subCat1', function(newValue, oldValue, scope)
		{
			console.log(newValue);
			$scope.cat3 = $scope.categories[x].children[newValue];
			console.log("cat3 ");
			console.log($scope.cat3);
			if ($scope.cat3.children.length == 0)
			{console.log("here");	
				$scope.categoryy = $scope.cat3;
				console.log($scope.categoryy);
				console.log($scope.subCat1);
				
			}
			console.log($scope.cat3);
		});
		
		$scope.$watch('subCat2', function(newValue, oldValue, scope)
		{
			if ($scope.subCat2 != null)
			{
				$scope.categoryy = $scope.cat3[newValue].title;
			}
		});
		$scope.checkUser = function(email, pass)
		{
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (re.test(email))
			{
				$http.get("/checkUser?email=" + email + "&&password=" + pass)
					.then(function(response)
					{
						$scope.wrongPass = !response.data.valid;
						if (!$scope.wrongPass)
						{
							$scope.loggedIn = true;
							$scope.loginUrl = "/profile#?email=" + email;
							$scope.loginSpan = response.data.doc.name;
							$cookies.put("lt", response.data.token);
							$scope.email = response.data.doc.email;
							$('#modal1')
								.modal('hide');
							//$cookies.put("user",$scope.email);
						}
					});
			}
			else
			{}
			/*	$http.get("/checkUser?email="+email+"&&password="+pass).then(function(response) {
								$scope.wrongPass= response.data.valid;
								if($scope.wrongPass){
								$window.location.href = '/profile';
								$cookies.put("user",$scope.email);
									}
							});

				*/
		};
		$scope.saveTags = function(keyEvent, a)
		{
			console.log("saveTags");
			if (a == 1)
			{
				if (keyEvent.which == 13)
				{
					console.log(keyEvent);
					if (($scope.tag != 'null')) $scope.tags.push($scope.tag);
					$scope.tag = null;
					var t = $scope.tags;
					$scope.tagss = t.join("$$");
				}
			}
			else if (a == 2)
			{
				console.log(keyEvent);
				$scope.tags.push(keyEvent.target.innerHTML);
				$scope.tag = null;
				var t = $scope.tags;
				$scope.tagss = t.join("$$");
			}
		};
		$scope.deleteTag = function(obj)
		{
			console.log(obj.target.attributes.id.value);
			var index = obj.target.attributes.id.value;
			console.log(index);
			var x = $scope.tags;
			x.splice(index, 1);
			$scope.tags = x;
			x = $scope.tagss;
			var y = x.split("$$");
			y.splice(index, 1);
			$scope.tagss = y.join("$$");
		};
		$scope.loadUser = function()
		{
			$scope.pincode = $cookies.get("pincode");
			$scope.uploaded = false;
			$scope.urls = false;
			$scope.name = null;
			$scope.price = null;
			$scope.course = null;
			$scope.year = null;
			$scope.names = false;
			$scope.tags = [];
			$scope.tagss = null;
			updateJSArrays([], [], 2);
			/*$scope.email = $cookies.get("user");
        if ($scope.email == null) {
            $window.location.href = '/login';
        }
        $http.get("/getUser?email=" + $scope.email)
            .then(function(response) {
                $scope.user = response.data;
            });*/
		};
		$scope.getTags = function()
		{
			console.log($scope.tag);
			$http.get("/getTags?tag=" + $scope.tag)
				.then(function(response)
				{
					console.log(response.data);
					$scope.tagsSuggestion = response.data;
				});
		};
		$scope.logout = function()
		{
			$cookies.remove("user");
			$scope.loadUser();
		};
		$scope.email = $cookies.get("user");
		$scope.updateRR = function(urls, names)
		{
			var t = urls.join("$$");
			$scope.urls = t;
			$scope.names = names;
			console.log($scope.urls);
			console.log($scope.names);
		}
		$scope.deleteImage = function(obj)
		{
			var index = obj.target.attributes.id.value;
			console.log(index);
			var x = $scope.names;
			x.splice(index, 1);
			var y = $scope.urls;
			var t = y.split("$$");
			t.splice(index, 1);
			$scope.names = x;
			$scope.urls = t.join("$$");
			updateJSArrays(x, t, 1);
			console.log($scope.urls);
			console.log($scope.names);
		}
		$scope.test = function(a)
		{
			if ($scope.category == '' || $scope.category == 'undefined' || $scope.category == null)
			{
				return "";
			}
			else
			{
				$http.get("/getChildren?title=" + a)
					.then(function(res)
					{
						console.log(res.data);
						return a;
					});
			}
		}
		$scope.submitBook = function()
		{
			$scope.uploaded = true;
			if (!$scope.tagss || !$scope.urls || !$scope.name || !$scope.price || !$scope.author || $scope.edition)
				$http.post("/save?tags=" + $scope.tagss + "&imgUrl=" + $scope.urls + "&name=" + $scope.name + "&price=" + $scope.price + "&email=" + $scope.email + "&lat=" + $scope.lat + "&lon=" + $scope.lon + "&city=" + $scope.city + "&author=" + $scope.author + "&desc=" + $scope.desc + "&category=" + $scope.category + "&bookType=" + $scope.bookType + "&edition=" + $scope.edition + "&pageId=" + $scope.pageId + "&fd=" + $scope.fd)
				.then(function(response)
				{
					$scope.uploadStatus = response.data;
				});
		}
	});
	app.controller("addExistingCtrl", function($scope, $http, $cookies, $window)
	{
		$http.get("/allCategories")
			.then(function(response)
			{
				console.log(response.data);
				$scope.categories = response.data;
			});
		$scope.searchBooks = function(event, x)
		{
			if (event.which == 13)
			{
				if (x == 1)
				{
					console.log("Searching from tags");
					var x = $scope.searchTags;
					$http.get("/getPages?data=" + x + "&type=1")
						.then(function(response)
						{
							console.log(response.data);
							$scope.results = response.data;
						});
				}
				else if (x == 2)
				{
					console.log("Searching from title");
					var x = $scope.searchTitle;
					$http.get("/getPages?data=" + x + "&type=2")
						.then(function(response)
						{
							console.log(response.data);
							$scope.results = response.data;
						});
				}
				else if (x == 3)
				{
					console.log("Searching from authors");
					var x = $scope.searchAuthor;
					$http.get("/getPages?data=" + x + "&type=3")
						.then(function(response)
						{
							console.log(response.data);
							$scope.results = response.data;
						});
				}
			}
		};
		$scope.getCategory = function(id)
		{
			$scope.categories.forEach(function(x)
			{
				if (x._id == id)
				{
					console.log(x.title);
					var y = x.title;
					return y;
				}
			});
		}
		$scope.sell = function(event)
		{
			$window.location.href = "/addNewBook#?pageId=" + event.target.attributes.id.value;
		};
	});
	app.controller("searchAllCtrl", function($scope, $http, $window, $location, $sce, $timeout)
	{
		var z = "";
		$scope.fd1 = true;
		$scope.fd2 = true;
		$scope.fdc1 = true;
		$scope.fdc2 = true;
		$scope.ob2 = true;
		$scope.ob1 = true;
		$scope.obc2 = true;
		$scope.obc1 = true;
		$scope.priceFilter = 1500;
		$scope.testFilter = function()
		{
			console.log("start");
			console.log("fd1 " + $scope.fd1);
			console.log("fd2 " + $scope.fd2);
			console.log("ob1 " + $scope.ob1);
			console.log("ob2 " + $scope.ob2);
		};
		$scope.load = function()
		{
			$scope.skip = 0;
			$scope.reqSent = false;
			$scope.searchText = $location.$$search.searchText;
			$http.get("/all?searchText=" + $scope.searchText + "&&skip=" + $scope.skip + "&priceFilter=9999999999&type=2")
				.then(function(response)
				{
					console.log("inside all");
					var z = "";
					var x = response.data;
					console.log(x);
					x.forEach(function(y)
					{
						console.log(y);
						z += '<li class="feedcard" ><div class="bsf-shadow feedcardinner"><img class="feedcardimg" src="' + y.images[0] + '"><div class="readlaterbtn" data-toggle=snackbar data-content=\'Added to: "Books I want to read"\'><span class="fa fa-book"></span></div><p class="zmargin feedtitle">' + y.title + '</p><p class="cost cardcostrs zmargin">Rs. </p><p class="cost zmargin cardcost">' + y.price + '</p><div class="tagscontainer"><p><div class="row zpadding zmargin tagfontsize"><div class="col-md-6 col-sm-6 col-xs-12"><div class="educationtag tag">' + y.category + '</div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="tag ' + y.type + 'tag">' + y.type + '</div></div></div></p></div><div id="container" class="cardlikecontainer"><div class="heart cardheart" id="like1" rel="like"></div><div class="likeCount cardlikecount" id="likeCount1">1000</div></div></div></li>';
						$scope.appendData = $sce.trustAsHtml(z);
					});
					$timeout(ao, 0);
				});
		};
		$scope.appendFeedItems = function()
		{
			console.log("appendFeedItems");
			if (!$scope.reqSent)
			{
				$scope.skip += 10;
				$scope.reqSent = true;
				$http.get("/all?searchText=" + $scope.searchText + "&skip=" + $scope.skip + "&priceFilter=" + $scope.priceFilter + "&fd=" + $scope.fd + "&ob=" + $scope.ob)
					.then(function(response)
					{
						$scope.reqSent = false;
						var x = response.data;
						x.forEach(function(y)
						{
							console.log(y);
							z += '<li class="feedcard" ><div class="bsf-shadow feedcardinner"><img class="feedcardimg" src="' + y.images[0] + '"><div class="readlaterbtn" data-toggle=snackbar data-content=\'Added to: "Books I want to read"\'><span class="fa fa-book"></span></div><p class="zmargin feedtitle">' + y.title + '</p><p class="cost cardcostrs zmargin">Rs. </p><p class="cost zmargin cardcost">' + y.price + '</p><div class="tagscontainer"><p><div class="row zpadding zmargin tagfontsize"><div class="col-md-6 col-sm-6 col-xs-12"><div class="educationtag tag">' + y.category + '</div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="tag ' + y.type + 'tag">' + y.type + '</div></div></div></p></div><div id="container" class="cardlikecontainer"><div class="heart cardheart" id="like1" rel="like"></div><div class="likeCount cardlikecount" id="likeCount1">1000</div></div></div></li>';
							//$scope.appendData = $sce.trustAsHtml(z);
							$scope.appendData = z;
						});
					});
			}
		};
		$scope.getBooks = function()
			{
				console.log("inside all");
				var fd1 = $scope.fd1;
				var fd2 = $scope.fd2;
				var ob1 = $scope.ob1;
				var ob2 = $scope.ob2;
				if ((fd1 && fd2) || (!fd1 && !fd2))
				{
					$scope.fd = "''";
				}
				else if (fd1)
				{
					$scope.fd = true;
				}
				else
				{
					$scope.fd = false;
				}
				if ((ob1 && ob2) || (!ob1 && !ob2))
				{
					$scope.ob = "''";
				}
				else if (ob1)
				{
					$scope.ob = true;
				}
				else
				{
					$scope.ob = false;
				}
				$scope.skip = 0;
				$scope.appendData = "";
				z = "";
				$http.get("/all?searchText=" + $scope.searchText + "&skip=0" + "&priceFilter=" + $scope.priceFilter + "&fd=" + $scope.fd + "&ob=" + $scope.ob + "&type=1")
					.then(function(response)
					{
						var x = response.data;
						x.forEach(function(y)
						{
							console.log(y);
							z += '<li class="feedcard" ><div class="bsf-shadow feedcardinner"><img class="feedcardimg" src="' + y.images[0] + '"><div class="readlaterbtn" data-toggle=snackbar data-content=\'Added to: "Books I want to read"\'><span class="fa fa-book"></span></div><p class="zmargin feedtitle">' + y.title + '</p><p class="cost cardcostrs zmargin">Rs. </p><p class="cost zmargin cardcost">' + y.price + '</p><div class="tagscontainer"><p><div class="row zpadding zmargin tagfontsize"><div class="col-md-6 col-sm-6 col-xs-12"><div class="educationtag tag">' + y.category + '</div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="tag ' + y.type + 'tag">' + y.type + '</div></div></div></p></div><div id="container" class="cardlikecontainer"><div class="heart cardheart" id="like1" rel="like"></div><div class="likeCount cardlikecount" id="likeCount1">1000</div></div></div></li>';
							$scope.appendData = $sce.trustAsHtml(z);
						});
						$timeout(ao, 0);
					});
			}
			//EOC
	});
	app.controller("shopCtrl", function($scope, $http, $location)
	{
		var absUrl = $location.absUrl();
		var a = absUrl.split("/");
		var username = a[3];
		console.log(username);
		$http.get("/ltu?username=" + username)
			.then(function(res)
			{
				console.log(res.data);
				$scope.books = res.data;
			});
	});
