$(function () {


	$('#hero .flexslider').flexslider({
		start: renderPreview,	//render preview on start
		before: renderPreview, //render preview before moving to the next slide
		manualControls: ".flex-control-nav li",
		slideshow: true,
		animationSpeed: 600,
		slideshowSpeed: 3000,
		animationLoop: true,
		//controlsContainer: $(".flexslider-controls"),
		//customDirectionNav: $(".flex-control-nav"),
		useCSS: false /* Chrome fix*/
	});

	function renderPreview(slider) {

		 var sl = $(slider);
		 var prevWrapper = sl.find('.flex-prev');
		 var nextWrapper = sl.find('.flex-next');

		 //calculate the prev and curr slide based on current slide
		 var curr = slider.animatingTo;
		 var prev = (curr == 0) ? slider.count - 1 : curr - 1;
		 var next = (curr == slider.count - 1) ? 0 : curr + 1;

		 //add prev and next slide details into the directional nav
		 prevWrapper.find('.preview, .arrow').remove();
		 nextWrapper.find('.preview, .arrow').remove();
		 prevWrapper.append(grabContent());
		 nextWrapper.append(grabContent());

	}

	function grabContent() {
		html = '<div class="arrow" style="z-index: 1000 !important; "></div>';
		return html;
	}

});
