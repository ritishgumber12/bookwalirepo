var mongoose = require("mongoose");
var adminSchema = {
	quote:
	{
		type: String
	},
	slider: [
	{
		image: String,
		target: String
	}],
	category: [
	{
		image: String,
		target: String,
		name: String
	}],
};
var schema = new mongoose.Schema(adminSchema);
module.exports = schema;
module.exports.adminSchema = schema;
