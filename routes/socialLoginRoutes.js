module.exports= function(app,passport){	

	app.get('/auth/facebook', passport.authenticate('facebook',
	{
		scope: 'email'
	}));
	// handle the callback after facebook has authenticated the user
	app.get('/auth/facebook/callback',
		passport.authenticate('facebook',
		{
		//	successRedirect: '/',
			failureRedirect: '/error',
			  session: false

		}),function(req,res){

				var token=req.user.generateJwt();
				console.log(token);
				res.redirect("/addCookie#?token="+token);
				//res.send("hy");
			});
	app.get("/addCookie",function(req,res){
		res.render("./addCookie.html")
	});
	app.get('/auth/google', passport.authenticate('google',
	{
		scope: ['profile', 'email']
	}));
	app.get('/auth/google/callback',
		passport.authenticate('google',
		{
  				session: false,
				failureRedirect: '/error'
		}),function(req,res){
		
				var token=req.user.generateJwt();
				console.log(token);
				res.redirect("/addCookie#?token="+token);
				//res.send("hy");
			});
	app.get('/auth/instagram',
		passport.authenticate('instagram'));
	app.get('/auth/instagram/callback',
		passport.authenticate('instagram',
		{
			failureRedirect: '/error',
			  session: false

		}),function(req,res){
		
				var token=req.user.generateJwt2('instagram');
				console.log(token);
				res.redirect("/addCookie#?token="+token);
				//res.send("hy");
			});
	app.get('/auth/twitter',
		passport.authenticate('twitter'),function(){});
	app.get('/auth/twitter/callback',
		passport.authenticate('twitter',
		{
			failureRedirect: '/auth/twitter',
			  session: false

		}),function(req,res){
		
				var token=req.user.generateJwt2('twitter');
				//var token="sdd";
				console.log(token);
				res.redirect("/addCookie#?token="+token);
				//res.send("hy");
			});
	app.get('/auth/goodreads',
		passport.authenticate('goodreads'));
	app.get('/auth/goodreads/callback',
		passport.authenticate('goodreads',
		{
  		session: false,
				failureRedirect: '/error'
		}),function(req,res){
		
				var token=req.user.generateJwt2('goodreads');
				console.log(token);
				res.redirect("/addCookie#?token="+token);
				//res.send("hy");
			});
};