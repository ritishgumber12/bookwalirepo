function validateaddrpin(){
	if($("#addrinput").val()!="1"){
		$("#detectpin").html(' 110033');
	}else{
		$("#detectpin").html(' Unable to detect');
	}
}
function setWindowVariable(x){
window.utrue=x;
}
function validateusername(x){
		if(x){
			$(".usernamewarningtext").find('i').removeClass("fa-times-circle");
			$(".usernamewarningtext").find('i').removeClass("fa-check-circle");
			$(".usernamewarningtext").find('i').addClass("fa-check-circle");
			$(".usernamewarningtext").find('span').html(' Username available');
			$(".usernamewarningtext").css({"color":"#27AE60","display":"block"});
			window.utrue=1;
		}else{
			$(".usernamewarningtext").find('i').removeClass("fa-times-circle");
			$(".usernamewarningtext").find('i').removeClass("fa-check-circle");
			$(".usernamewarningtext").find('i').addClass("fa-times-circle");
			$(".usernamewarningtext").find('span').html(' Username not available');
			$(".usernamewarningtext").css({"color":"#c61331","display":"block"});
			window.utrue=0;
		}
}
function setwarning(tid,m){
	$("#"+tid).find('span').html(" "+m);
	$("#"+tid).css({"display":"block"});
}
function hidewarning(tid){
	$("#"+tid).find('span').html(" ");
	$("#"+tid).css({"display":"none"});
}

$(document).ready(function(){
	$('#nameinput').keyup(function(){
		$('#cardnametarget').html($(this).val());
	});
	$("option:selected").removeAttr("selected");
	$(".formwarningtext").css({"display":"none"});
	$(".usernamewarningtext").find("span").html(" Username not available.");

var current_fs, next_fs, previous_fs;
$(".next").click(function(){
	if($(this).hasClass("basicdetails")){
		if($("#nameinput").val()!=''){
			if(($("#mailinput").val()!='')&&(window.mailtrue==1)){
				if($("#passinput").val()!=''){
					if($("#cnoinput").val()!=''){
						if(($("#usernameinput").val()!='')&&(window.utrue==1)){
							$("#basicdetailswarningtext").css({"display":"none"});
							current_fs = $(this).parent();
							next_fs = $(this).parent().next();
							$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
						      current_fs.hide();
						      next_fs.show();
						}else{
							setwarning("basicdetailswarningtext","Enter a valid username.")
						}
					}else{
							setwarning("basicdetailswarningtext","Enter your contact no.")
					}
				}else{
					setwarning("basicdetailswarningtext","Enter a new password.")
				}
			}else{
				setwarning("basicdetailswarningtext","Enter your Email.")
			}
		}else{
			setwarning("basicdetailswarningtext","Enter your name.")
		}
	}else if($(this).hasClass("interestdetails")){
		if($("input[name=selltyperadio]:checked").length<=0){
				setwarning("interestdetailswarningtext","Chose your profile type.")
		}else{
			if($("select[name='interestchoices'] option:selected").length<=0){
				setwarning("interestdetailswarningtext","Select your interest categories.")
			}else{
				$("interestdetailswarningtext").css({"display":"none"});
				current_fs = $(this).parent();
				next_fs = $(this).parent().next();
				$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
						current_fs.hide();
						next_fs.show();
			}

		}

	}else if($(this).hasClass("otherdetails")){

	}
});

$(".previous").click(function(){
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
  current_fs.hide();
  previous_fs.show();
});


$(".submit").click(function(){
	return true;
});
});
